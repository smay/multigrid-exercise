#!/usr/bin/env python3
import numba
import numpy
import scipy.ndimage
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

G = 1
L = 1.0
ETA = 0.1 * L
RHO_0 = 10.0
N = 256
H = L / N
N_STEPS = 2000

@numba.njit
def poisson_stencil(x, b, i, j):
	N = len(x)
	return 1/4 * (
		x[(i - 1) % N, j] + x[(i + 1) % N, j] +
		x[i, (j - 1) % N] + x[i, (j + 1) % N] - b[i, j]
	)

### part 2a)
def jacobi_step_convolve(x, b):
	x_new = scipy.ndimage.convolve(x,
		[[0, 1, 0],
		[1, -4, 1],
		[0, 1, 0]],
		mode='wrap'
	)
	x_new -= b
	x_new /= 4
	return x_new

@numba.njit
def jacobi_step_numba(x, b):
	N = len(x)
	x_new = numpy.empty_like(x)
	for i in range(N):
		for j in range(N):
			x_new[i, j] = poisson_stencil(x, b, i, j)
	return x_new

def jacobi_step_roll(x, b):
	x_im1 = numpy.roll(x, -1, axis=0)
	x_ip1 = numpy.roll(x, 1, axis=0)
	x_jm1 = numpy.roll(x, -1, axis=1)
	x_jp1 = numpy.roll(x, 1, axis=1)
	return 1/4 * (x_im1 + x_ip1 + x_jm1 + x_jp1 - b)

def jacobi_step_slice(x, b):
	x_new = numpy.zeroes_like(x)
	x_new[1:, :] += x[:-1, :]
	x_new[0, :] += x[-1, :]
	x_new[:-1, :] += x[1:, :]
	x_new[-1, :] += x[0, :]
	x_new[:, 1:] += x[:, :-1]
	x_new[:, 0] += x[:, -1]
	x_new[:, :-1] += x[:, 1:]
	x_new[:, -1] += x[:, 0]
	x_new -= b
	x_new /= 4
	return x_new

jacobi_step = jacobi_step_numba

### part 2b)
@numba.njit
def residual(x, b):
	N = len(x)
	Ax = numpy.empty_like(x)
	for i in range(N):
		for j in range(N):
			Ax[i, j] = -4 * x[i, j] + (
				x[(i - 1) % N, j] + x[(i + 1) % N, j] +
				x[i, (j - 1) % N] + x[i, (j + 1) % N]
			)
	return b - Ax

def residual_norm(r):
	return numpy.sqrt(numpy.sum(r**2))

### part 2c)
def initialize_density():
	x = y = numpy.linspace(-L/2, L/2, N)
	X, Y = numpy.meshgrid(x, y, sparse=True)
	return RHO_0 * numpy.exp(-(X**2 + Y**2) / (2 * ETA**2))

@numba.jit
def iteration(iteration_step):
	b = 4 * numpy.pi * G * H**2 * initialize_density()
	b -= numpy.mean(b)
	phi = numpy.zeros((N, N))
	S = numpy.empty(N_STEPS)
	for i in range(N_STEPS):
		phi = iteration_step(phi, b)
		S[i] = residual_norm(residual(phi, b))
	return phi, S

### part 2d)
@numba.njit
def gauss_seidel_step(x, b):
	N = len(x)
	for i in range(N):
		for j in range(N):
			x[i, j] = poisson_stencil(x, b, i, j)
	return x

### part 2e)
@numba.njit
def gauss_seidel_red_black_step(x, b):
	N = len(x)
	for k in 0, 1:
		for i in range(N):
			for j in range((i + k) % 2, N, 2):
				x[i, j] = poisson_stencil(x, b, i, j)
	return x

### part 2f)
@numba.njit
def do_restrict(fine):
	N = len(fine)
	NN = (N + 1) // 2
	# currently no nice way to do this for arbitrary array shapes
	# https://github.com/numba/numba/issues/2771
	coarse = numpy.empty((NN, NN))
	f = fine
	for i in range(NN):
		i2 = 2 * i
		for j in range(NN):
			j2 = 2 * j
			coarse[i, j] = 1/16 * (
				f[(i2 - 1) % N, (j2 - 1) % N] + 2 * f[(i2 - 1), j2] +
					f[(i2 - 1) % N, j2 + 1] +
				2 * f[i2, (j2 - 1) % N] + 4 * f[i2, j2] + 2 * f[i2, j2 + 1] +
				f[i2 + 1, (j2 - 1) % N] + 2 * f[i2 + 1, j2] + f[i2 + 1, j2 + 1]
			)
	return coarse

### part 2g)
@numba.njit
def do_prolong(coarse):
	NN = len(coarse)
	N = 2 * NN
	# currently no nice way to do this for arbitrary array shapes
	# https://github.com/numba/numba/issues/2771
	fine = numpy.empty((N, N))
	c = coarse
	for i in range(NN):
		i2 = 2 * i
		for j in range(NN):
			j2 = 2 * j
			fine[i2, j2] = c[i, j]
			fine[i2, j2 + 1] = 1/2 * (c[i, j] + c[i, (j + 1) % NN])
			fine[i2 + 1, j2] = 1/2 * (c[i, j] + c[(i + 1) % NN, j])
			fine[i2 + 1, j2 + 1] = 1/4 * (
				c[i, j] + c[i, (j + 1) % NN] + c[(i + 1) % NN, j] +
				c[(i + 1) % NN, (j + 1) % NN]
			)
	return fine

### part 2h)
@numba.njit
def do_v_cycle(x, b):
	N = len(x)
	x = gauss_seidel_step(x, b)
	if N > 4:
		NN = (N + 1) // 2
		r = residual(x, b)
		r_coarse = 4 * do_restrict(r)
		x_err = do_v_cycle(numpy.zeros((NN, NN)), r_coarse)
		x_err_fine = do_prolong(x_err)
		x += x_err_fine
	x = gauss_seidel_step(x, b)
	return x

### main execution/output
plt.figure()
plt.yscale('log')
ax = plt.gca()
for iteration_step in (
	('Jacobi iteration', jacobi_step),
	('Gauss–Seidel iteration', gauss_seidel_step),
	('Gauss–Seidel iteration (red–black ordering)',
		gauss_seidel_red_black_step),
	('Multigrid', do_v_cycle),
):
	step_name, step_fun = iteration_step
	phi, S = iteration(step_fun)
	print(f'∑ Φ = {numpy.sum(phi)} ({step_name})')

	ax.plot(S, label=step_name)
	plt.figure()
	plt.title(step_name)
	x = y = numpy.arange(N)
	X, Y = numpy.meshgrid(x, y)
	plt.pcolormesh(X, Y, phi)
	plt.colorbar()

ax.legend()
ax.set_ylim(4e-3, 2e-1)
plt.show()

